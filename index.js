const express = require('express');
const app = express();
const morgan = require('morgan');
// Settings

app.set('appName', 'Daimon Express');
app.set('port', 3000);
app.set('view engine', 'ejs');

// Middlewares
app.use(express.json());
app.use(morgan('dev'));



//routes

/*app.all('/user', (req, res, next) => {
      console.log('Por aqui paso');
      next();
})*/


app.get('/', (req, res) => {
    res.render('index.ejs');

});

app.get('/user', (req, res) => {
   res.json({
       username: "Cameron",
       lastname: "Diaz"
   });

});

app.post('/user/:id', (req, res) => {
    console.log(req.body);
    console.log(req.params);
    res.send('Post recibida');

});

app.put('/user/:id', (req, res) => {
   console.log(req.body);
   res.send('User ${req,params.id} updated');

});

app.delete('/user/:userId', (req, res) => {
   res.send('userid ${req.params.userId} deleted');

});


app.use(express.static('public'));


app.listen(app.get('port'), () => {

   console.log(app.get('appName'));
   console.log('Server on port', app.get('port'));

});

